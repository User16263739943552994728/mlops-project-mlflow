#!/bin/sh

mlflow server \
    --backend-store-uri postgresql://postgres:mysecretpassword@db:5432/mlflow_db \
    --default-artifact-root s3://mlflow-artifacts \
    --host 0.0.0.0
