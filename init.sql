DO $$ 
BEGIN
   IF NOT EXISTS (SELECT 1 FROM pg_database WHERE datname = 'mlflow_db') THEN
      CREATE DATABASE mlflow_db;
   END IF;
END
$$;
